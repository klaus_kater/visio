import {setScene, sceneMap, visCanvas} from "./rendering.js"
import {toogleMask} from "./mask.js"

export default function(ratio){
  const windowButton = document.getElementById("newWindow")
  const maskActivator = document.getElementById("maskActivator")
  const menu = document.getElementsByClassName("menu")[0]

  maskActivator.onclick = toogleMask

  windowButton.onclick = ()=>{
    const newWin = window.open('window.html', '', 'width=600,height=400')
    newWin.onload = ()=>{
      newWin.document.body.appendChild( visCanvas )
      newWin.ratio = ratio
    }
  }

  for(let scene in sceneMap){
    const button = document.createElement("Button")
    button.innerHTML = scene
    button.onclick = ()=>{setScene(sceneMap[scene])}
    menu.appendChild(button)
  }
  // maskActivator
}