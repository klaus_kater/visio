import BaseShader from "../scenes/base.js"
import ShelfShader from "../scenes/shleif.js"
import FreezeShader from "../scenes/freeze.js"
import SmokeShader from "../scenes/smoke.js"
import TailShader from "../scenes/tail.js"

let frame = {
  width: undefined,
  height: undefined
}
const videoSource = {
  width: undefined,
  height: undefined,
  ratio: undefined,
  texture: undefined
}

const maskSource = {}

export const sceneMap = {
  "Без эффектов": BaseShader,
  "Фриз": FreezeShader,
  "Шлейф": ShelfShader,
  "Дым": SmokeShader,
  "Хвост": TailShader
}

let video
let videoCtx
let mask = document.getElementById('maskCanvas')
export const visCanvas = document.createElement("Canvas")
visCanvas.id = "visCanvas"
const visCanvasCtx = visCanvas.getContext("2d")
let stats
// let video_texture

// let streaming = false
let scene
let camera
let renderer
let renderer2
let plane
let activeScene

function initSizeVisible(){
  if(!videoSource.ratio) return

  const rendCanvas = document.getElementById('renderer')

  let clientWidth = document.documentElement.clientWidth - 10
  let clientHeight = document.documentElement.clientHeight - 56 // костыль..

  if(clientHeight * videoSource.ratio > clientWidth){
    clientHeight = Math.floor(clientWidth / videoSource.ratio)
  }else{
    clientWidth = Math.floor(clientHeight * videoSource.ratio)
  }
  rendCanvas.style.width = `${clientWidth}px`
  rendCanvas.style.height = `${clientHeight}px`
  
  // renderer.domElement.style.setProperty('width', clientWidth, 'important')
  //   renderer.domElement.style.setProperty('height', clientHeight, 'important')
  // renderer.domElement.height = clientHeight
  
  // renderer2.setSize( frame.width, frame.height )
}

window.addEventListener("optimizedResize", function() {
  initSizeVisible()
})

function videoInit(){
  // var video = document.getElementById('video')
  // videoSource.texture = new THREE.Texture( video )
  // 
  // при переходе на видео - перерисовка в канвас
  // https://stackoverflow.com/questions/4429440/html5-display-video-inside-canvas
  
  video = document.getElementById('source')

  videoCtx = video.getContext("2d")

  videoSource.texture = new THREE.Texture(video)
  videoSource.texture.minFilter = THREE.LinearFilter

  videoSource.width = video.width // На канвасе width на видео videoWidth
  videoSource.height = video.height

  videoSource.ratio = videoSource.width/videoSource.height

  let mask = document.getElementById('maskCanvas')

  maskSource.texture = new THREE.Texture(mask)
  maskSource.texture.minFilter = THREE.LinearFilter
  


  //   if (navigator.getUserMedia) {
  //      navigator.getUserMedia (
  //         {
  //            video: true,
  //            audio: false
  //         },

  //         function(localMediaStream) {
  //           video.setAttribute('autoplay', 'autoplay');

  //           video.src = window.URL.createObjectURL(localMediaStream); //HTMLMediaElement.srcObject// 

  //           video.addEventListener('canplay', function(ev) {
  //             if (!streaming) {

  //               videoSource.width = video.videoWidth
  //               videoSource.height = video.videoHeight

  //               videoSource.ratio = videoSource.width/videoSource.height

  //               streaming = true;

  //               keyboardSetup()

  //               initSizeVisible()
                
  //               scene_setup()

  //               sceneInit()
  //             }
  //           }, false);
  //         },

  //         // errorCallback
  //         function(err) {
  //            console.log("The following error occured: " + err);
  //         }
  //      );
  //   } else {
  //      console.log("Данный браузер не поддерживает захват видео");
  //   }
}
function sceneInit(){
  scene = new THREE.Scene()
  camera = new THREE.OrthographicCamera( videoSource.width / - 2, videoSource.width / 2, videoSource.height / 2, videoSource.height / - 2, 1, 1000 );
  camera.position.z = 2;

  plane = new THREE.PlaneBufferGeometry( videoSource.width, videoSource.height );

  renderer = new THREE.WebGLRenderer();
  renderer.setSize( videoSource.width, videoSource.height );
  renderer.domElement.id = "renderer"
  document.body.appendChild( renderer.domElement );

  renderer2 = new THREE.WebGLRenderer();
  renderer2.setSize( videoSource.width, videoSource.height );
}

export function setScene(sceneObj){
  activeScene = sceneObj

  activeScene.videoSource = videoSource
  activeScene.frame = {width: videoSource.width, height: videoSource.height }
  activeScene.plane = plane
  activeScene.scene = scene
  activeScene.camera = camera
  // activeScene.renderer = renderer

  activeScene.init()
}

function render() {
  stats.begin();

  videoCtx.drawImage(mask, 0, 0);

  videoSource.texture.needsUpdate = true;

  activeScene.render(renderer)
  // activeScene.render(renderer2)
  // 
  visCanvasCtx.drawImage(renderer.domElement, 0, 0);

  stats.end();

  requestAnimationFrame( render );
}

export default function(statsHandler){
  stats = statsHandler
  videoInit()
  sceneInit()
  initSizeVisible()
  setScene(BaseShader)
  render()
  return {width: videoSource.width, height:videoSource.height}
}