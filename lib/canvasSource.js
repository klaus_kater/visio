export default function SourceInit() {
  let canvas = document.getElementById('source')

  let ctx = canvas.getContext('2d');

  const topSh = canvas.offsetTop
  const leftSh = canvas.offsetLeft

  let onMouse = false
  let mouseX = 0;
  let mouseY = 0;

  function printCircle(color){
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(mouseX-leftSh, mouseY-topSh, 30, 0, Math.PI * 2);
    ctx.fill()
  }
  canvas.onmousedown = (ev)=>{
    onMouse = true
  }
  const end = (ev)=>{
    onMouse = false
  }
  canvas.onmouseup = end
  canvas.onmouseout = end

  canvas.onmousemove = (ev)=>{
    mouseX = ev.clientX
    mouseY = ev.clientY
  }

  function draw() {

    ctx.globalCompositeOperation = 'source-over'
    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    ctx.fill();

    onMouse && printCircle('rgba(255, 255, 255, 1)')

    window.requestAnimationFrame(draw)
  }
  window.requestAnimationFrame(draw)

}