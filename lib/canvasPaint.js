  let canvas = document.getElementById('source')

  let ctx = canvas.getContext('2d');

  let onMouse = false
  let mouseX = 0;
  let mouseY = 0;

  printCircle = (color) => {
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(mouseX, mouseY, 30, 0, Math.PI * 2);
    ctx.fill()
  }
  canvas.onmousedown = (ev)=>{
    onMouse = true
  }
  const end = (ev)=>{
    onMouse = false
  }
  canvas.onmouseup = end
  canvas.onmouseout = end

  canvas.onmousemove = (ev)=>{
    mouseX = ev.clientX
    mouseY = ev.clientY
  }

  function draw() {

    ctx.globalCompositeOperation = 'source-over'

    // ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    // ctx.fillStyle = 'rgba(200, 200, 200, 1)';
    // ctx.arc(50, 50, 30, 0, Math.PI * 2);
    ctx.fill();

    onMouse && printCircle('rgba(255, 255, 255, 1)')

    window.requestAnimationFrame(draw)
  }
  window.requestAnimationFrame(draw)
