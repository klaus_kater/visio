let active = false
let container
let mask
let panel
let sizeBrush
let clearButton

export function toogleMask(){
  if(!container && !mask) return
  panel.innerHTML = '';
  if(active){
    active = false
    container.style.display = "none"
    mask.style.opacity = 0
  } else {
    active = true
    container.style.display = "block"
    mask.style.opacity = 0.5

    panel.appendChild(sizeBrush)
    panel.appendChild(clearButton)
  }
}

export default function(sourceSize) {
  container = document.getElementById('mask')
  mask = document.getElementById('maskCanvas')
  panel = document.getElementById('panel')
  let rendererDom = document.getElementById('renderer')

  clearButton = document.createElement("Button")
  clearButton.id = "clearButton"
  clearButton.innerText = "Очистить"
  clearButton.onclick = ()=>{
    ctx2.clearRect(0, 0, mask.width, mask.height)
  }

  sizeBrush = document.createElement("Input")
  sizeBrush.id = "sizeBrush"
  sizeBrush.width = "30px"
  sizeBrush.type = "range"
  sizeBrush.min = "1"
  sizeBrush.max = "100"
  sizeBrush.step = "1"

  // режимы
  // рисование маски
  // - очистка
  // - размер кисти
  // наложение маски
  // нет маски

  mask.width = sourceSize.width
  mask.height = sourceSize.height

  mask.style.top = rendererDom.offsetTop+"px"
  mask.style.left = rendererDom.offsetLeft+"px"

  mask.style.width = `${rendererDom.offsetWidth}px`
  mask.style.height = `${rendererDom.offsetHeight}px`

  let ratiox = sourceSize.width / rendererDom.offsetWidth
  let ratioy = sourceSize.height / rendererDom.offsetHeight

  window.addEventListener("optimizedResize", function() {
    mask.style.width = `${rendererDom.offsetWidth}px`
    mask.style.height = `${rendererDom.offsetHeight}px`

    ratiox = sourceSize.width / rendererDom.offsetWidth
    ratioy = sourceSize.height / rendererDom.offsetHeight
  })

  // addEventListener("keydown", function(event) {
  //   switch(event.keyCode){     
  //     case 77:  // если нажата клавиша M
  //       if(!active){
  //         active = true
  //         container.style.display = "block"
  //         mask.style.opacity = 0.5
  //       } else {
  //         active = false
  //         container.style.display = "none"
  //         mask.style.opacity = 0
  //       }
  //   }
  // })

  let ctx2 = mask.getContext('2d')
  ctx2.globalCompositeOperation = 'source-over'

  let onMouse = false
  let mouseX = 0;
  let mouseY = 0;

  function printCircle2(color){
    ctx2.beginPath();
    ctx2.fillStyle = color;
    ctx2.arc((mouseX - rendererDom.offsetLeft) * ratiox, (mouseY - rendererDom.offsetTop) * ratioy, sizeBrush.value*ratiox, 0, Math.PI * 2);
    ctx2.fill()
  }
  mask.onmousedown = (ev)=>{
    onMouse = true
  }
  const end = (ev)=>{
    onMouse = false
  }
  mask.onmouseup = end
  mask.onmouseout = end

  mask.onmousemove = (ev)=>{
    mouseX = ev.clientX
    mouseY = ev.clientY
  }

  function draw() {

    // ctx.globalCompositeOperation = 'source-over'

    // ctx.clearRect(0, 0, mask.width, mask.height)
    // ctx.fillStyle = 'rgba(1, 1, 1, 1)';
    // ctx.fillRect(0, 0, mask.width, mask.height)
    // ctx.fillStyle = 'rgba(200, 200, 200, 1)';
    // ctx.arc(50, 50, 30, 0, Math.PI * 2);
    // ctx.fill();

    onMouse && active && printCircle2("#000000"); //'rgba(0, 0, 1, 1)')

    window.requestAnimationFrame(draw)
  }
  window.requestAnimationFrame(draw)

}