// Detector.js

import Stats from "./node_modules/stats.js/src/Stats.js"
import SourceInit from "./lib/canvasSource.js"
import rendering from "./lib/rendering.js"
import panelButtons from './lib/panelButtons.js'
import maskInit from './lib/mask.js'
// scenes/shleif.js
// scenes/base.js
// scenes/freeze.js
// scenes/smoke.js
// canvasPaint.js
// 
var throttle = function(type, name, obj) {
    obj = obj || window;
    var running = false;
    var func = function() {
        if (running) { return; }
        running = true;
         requestAnimationFrame(function() {
            obj.dispatchEvent(new CustomEvent(name));
            running = false;
        });
    };
    obj.addEventListener(type, func);
};

/* init - you can init any event */
throttle("resize", "optimizedResize");

window.addEventListener("load", function() {
  var stats = new Stats()
  stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
  document.body.appendChild( stats.dom )
  stats.dom.style.top = "41px"

  SourceInit()
  const size = rendering(stats)

  panelButtons(size.width/size.height)
  maskInit(size)

})

// window.onload = function() {
//   var stats = new Stats()
//   stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
//   document.body.appendChild( stats.dom )

//   SourceInit()
//   rendering(stats)
// }