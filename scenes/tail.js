
  const fragmentShader = [
      "uniform vec2 size; //The width and height of our screen",
      "uniform sampler2D bufferTexture; //Our input texture",
      "uniform sampler2D sourceTexture; ",
      "uniform float length;",
      // "uniform vec3 smokeSource; //The x,y are the posiiton. The z is the power/density",
      // "uniform float time;",
      "void main() {",
          "vec2 pixel = gl_FragCoord.xy / size.xy;",
          "gl_FragColor = texture2D( bufferTexture, pixel );",
          "float factor = 1.0;",

          "float minimum = 0.1;",
          "if(gl_FragColor.r <= minimum) factor = 0.0;",
          "gl_FragColor.rgb *= length * factor;",
          "gl_FragColor = max(gl_FragColor, texture2D( sourceTexture, pixel ));",
          "//           ",
       "}"].join( "\n" )

  let init = false

  // var video
  // var video_texture
  // var maxtex = 2048
  // var width // = Math.round(maxtex/8*1.3)//160 // подставить расчет отношения
  // var height // = Math.round(maxtex/8)//120
  var streaming = false

  var bufferScene;
  var textureA;
  var textureB;
  var sizeTexture;
  var bufferMaterial;
  var bufferObject;
  var finalMaterial;
  var quad;
  var rows;
  var columns;
  var shiftleft = 0;
  var shifttop = 0;
  var time = 0;

  var rows = 8
  var columns = 4
  let shleifLength = 0.95

  export default {
    name: 'Tail',
    // video_texture: null,
    videoSource: null,
    frame: null,
    plane: null,
    scene: null,
    renderer: null,
    camera: null,
    init: function() {

      this.scene.children = []
      
      bufferScene = new THREE.Scene();
      textureA = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
      textureB = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});

      bufferMaterial = new THREE.ShaderMaterial( {
          uniforms: {
            bufferTexture: { type: "t", value: textureA },
            sourceTexture: { type: "t", value: this.videoSource.texture },
            size : {type: 'v2',value:new THREE.Vector2(this.videoSource.width, this.videoSource.height)},
            length : { type: "f", value: shleifLength },
          },
          fragmentShader: fragmentShader
      });
      bufferObject = new THREE.Mesh( this.plane, bufferMaterial );
      bufferScene.add(bufferObject);

      finalMaterial =  new THREE.MeshBasicMaterial({map: textureB});

      quad = new THREE.Mesh( this.plane, finalMaterial );
      this.scene.add(quad);

      init = true
    },
    render: function(renderer) {
      if(!init)return

      //Draw to textureB
      renderer.render(bufferScene,this.camera,textureB,true);
        
      //Swap textureA and B
      var t = textureA;
      textureA = textureB;
      textureB = t;
      quad.material.map = textureB;
      bufferMaterial.uniforms.bufferTexture.value = textureA;

      renderer.render( this.scene, this.camera );
    }
  }
