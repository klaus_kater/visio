  const fragmentShader = [
      "uniform vec2 size; //The width and height of our screen",
      "uniform sampler2D bufferTexture; //Our input texture",
      "uniform sampler2D sourceTexture; ",
      "uniform float freeze;",
      "void main() {",
          "vec2 pixel = gl_FragCoord.xy / size.xy;",
          "//Get the distance of the current pixel from the smoke source",
          // "float dist = distance(smokeSource.xy,gl_FragCoord.xy);",
          "//Get the color of the current pixel",
          "vec4 bufferColor = texture2D( bufferTexture, pixel );",
          "vec4 newColor = texture2D( sourceTexture, pixel ) * freeze;",
          "gl_FragColor = max(bufferColor, newColor);",
       "}"].join( "\n" )

  let init = false

  // var video
  // var video_texture
  // var maxtex = 2048
  // var width // = Math.round(maxtex/8*1.3)//160 // подставить расчет отношения
  // var height // = Math.round(maxtex/8)//120
  var streaming = false

  var bufferScene;
  var textureA;
  var textureB;
  var sizeTexture;
  var bufferMaterial;
  var bufferObject;
  var finalMaterial;
  var quad;

  var clearTexture = new THREE.WebGLRenderTarget();

  var freeze = 1
  let panel
  let clearButton

export default {
    name: 'Freeze',
    // video_texture: null,
    videoSource: null,
    frame: null,
    plane: null,
    scene: null,
    renderer: null,
    camera: null,
    init: function() {
      // this.scene = new THREE.Scene()
      // this.scene.children = []
      function clear() {
        if(freeze){
          bufferMaterial.uniforms.bufferTexture.value = clearTexture;
        }
        bufferMaterial.uniforms.freeze.value = freeze;
      }

      panel = document.getElementById('panel')
      panel.innerHTML = '';
      clearButton = document.createElement("Button")
      clearButton.id = "clearButton"
      clearButton.innerText = "Очистить (enter)"
      clearButton.onclick = ()=>{
        clear()
      }
      panel.appendChild(clearButton)

      addEventListener("keydown", event=>{
        if(event.keyCode == 13){
          clear()
        }
      });
      
      // const width = this.videoSource.width
      // const heigth = this.videoSource.height
      bufferScene = new THREE.Scene();
      textureA = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
      textureB = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});

      bufferMaterial = new THREE.ShaderMaterial( {
          uniforms: {
            bufferTexture: { type: "t", value: textureA },
            sourceTexture: { type: "t", value: this.videoSource.texture },
            size : {type: 'v2',value:new THREE.Vector2(this.videoSource.width, this.videoSource.height)},
            freeze: {type: 'f', value: freeze}
            // dimension: {type: 'v2',value:new THREE.Vector2(columns, rows)},
            // pos : {type: 'v2',value:new THREE.Vector2(shiftleft, shifttop)},
          },
          fragmentShader: fragmentShader
      });
      
      bufferObject = new THREE.Mesh( this.plane, bufferMaterial );
      bufferScene.add(bufferObject);

      // finalMaterial =  new THREE.MeshBasicMaterial({map: textureB});

      // quad = new THREE.Mesh( this.plane, finalMaterial );
      // this.scene.add(quad);

      init = true
    },
    render: function(renderer) {
      if(!init)return

      //Draw to textureB
      renderer.render(bufferScene,this.camera,textureB,true);
        
      //Swap textureA and B
      var t = textureA;
      textureA = textureB;
      textureB = t;
      // quad.material.map = textureB;
      bufferMaterial.uniforms.bufferTexture.value = textureA;

      renderer.render( bufferScene, this.camera );
    }
  }
