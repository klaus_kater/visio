
const fragShaderBuffer = [
    "uniform vec2 size; //The width and height of our screen",
    "uniform vec2 sizeFrame;",
    "uniform vec2 ratio;",
    "uniform vec2 dimension;",
    "uniform sampler2D texture;",
    "uniform vec2 allsize;",

    "void main() {",
      "vec2 coord = gl_FragCoord.xy;",

      "gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);",

      "for(int i=0;i<4;++i)",
      "{",
        "for(int j=0;j<8;++j)",
        "{",
          "vec4 color = texture2D( texture,(coord*ratio)/(size*dimension*dimension) + vec2(1.0/4.0*float(i), 1.0/8.0*float(j)));",
          "gl_FragColor = max(gl_FragColor, color);",
        "}",
      "}",
    "}"
  ].join( "\n" )

  const fragShaderSum = [

    "uniform vec2 size; //The width and height of our screen",
    "uniform sampler2D bufferTexture;",
    "uniform sampler2D sourceTexture;",

    "uniform vec3 smokeSource; //The x,y are the posiiton. The z is the power/density",
    "uniform vec2 dimension;",
    "uniform vec2 pos;",

    "void main() {",
      "vec2 coord = gl_FragCoord.xy; // / size.xy; // - pos.xy;",
      "vec2 position = size * pos;",

      "vec4 texel = vec4(0.0, 0.0, 0.0, 1.0);",

      "if(coord.x<(position.x + size.x) && coord.x>position.x",
         "&&  coord.y<(position.y + size.y) && coord.y>position.y)",
        "{",
        "texel = texture2D( sourceTexture, (coord - position)/size);",
      "}else{",
        "texel = texture2D( bufferTexture, coord/(size*dimension));",
      "}",

      "gl_FragColor = texel;",

    "}"].join( "\n" )

let init = false

var video
var video_texture
var maxtex = 2048
var width = Math.round(maxtex/8*1.3)//160 // подставить расчет отношения
var height = Math.round(maxtex/8)//120
var streaming = false

var bufferScene;
var textureA;
var textureB;
var sizeTexture;
var bufferMaterial;
var bufferObject;
var finalMaterial;
var quad;
var rows;
var columns;
var shiftleft = 0;
var shifttop = 0;
var time = 0;

var rows = 8
var columns = 4

export default {
  name: 'Shelf',
  // video_texture: null,
  videoSource: null,
  frame: null,
  plane: null,
  scene: null,
  renderer: null,
  camera: null,
  init: function() {

    this.scene.children = []

    bufferScene = new THREE.Scene();
    textureA = new THREE.WebGLRenderTarget( width*columns, height*rows, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
    textureB = new THREE.WebGLRenderTarget( width*columns, height*rows, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});

    bufferMaterial = new THREE.ShaderMaterial( {
        uniforms: {
          bufferTexture: { type: "t", value: textureA },
          sourceTexture: { type: "t", value: this.videoSource.texture },
          size : {type: 'v2',value:new THREE.Vector2(width, height)},
          dimension: {type: 'v2',value:new THREE.Vector2(columns, rows)},
          pos : {type: 'v2',value:new THREE.Vector2(shiftleft, shifttop)},
        },
        fragmentShader: fragShaderSum
    });
    // plane = new THREE.PlaneBufferGeometry( clientWidth, clientHeight );
    bufferObject = new THREE.Mesh( this.plane, bufferMaterial );
    bufferScene.add(bufferObject);

    // sizeTexture = new THREE.Texture()


    finalMaterial =  new THREE.ShaderMaterial( {
      uniforms: {
       texture: { type: "t", value: textureB },
       dimension: {type: 'v2',value:new THREE.Vector2(columns, rows)},
       size : {type: 'v2',value:new THREE.Vector2(width, height)}, 
       sizeFrame: {type: 'v2',value:new THREE.Vector2(this.frame.width, this.frame.height)}, 
       ratio: {type: 'v2',value:new THREE.Vector2((width*columns)/this.frame.width, (height*rows)/this.frame.height)}
      },
      fragmentShader: fragShaderBuffer
    });

    // console.log((width*columns)/clientWidth, (height*rows)/clientHeight)

    // finalMaterial =  new THREE.MeshBasicMaterial({map: textureB});

    quad = new THREE.Mesh( this.plane, finalMaterial );
    this.scene.add(quad);

    init = true
  },
  render: function(renderer) {
    if(!init)return

    //Draw to textureB
    renderer.render(bufferScene,this.camera,textureB,true);
      
    //Swap textureA and B
    var t = textureA;
    textureA = textureB;
    textureB = t;
    quad.material.map = textureB;
    bufferMaterial.uniforms.bufferTexture.value = textureA;

    bufferMaterial.uniforms.pos.value.x +=1
    if(bufferMaterial.uniforms.pos.value.x>= columns) {

      bufferMaterial.uniforms.pos.value.y +=1
      if(bufferMaterial.uniforms.pos.value.y>= rows)
        bufferMaterial.uniforms.pos.value.y = 0

      bufferMaterial.uniforms.pos.value.x = 0
    }
    renderer.render( this.scene, this.camera );
  }
}
