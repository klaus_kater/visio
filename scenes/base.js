
const fragmentShader = [
  // "varying vec2 vUv;",

  "uniform sampler2D texture;",
  "uniform vec2 size;",

  "void main() {",
    "vec2 coord = gl_FragCoord.xy;",

    "vec4 texel = texture2D( texture, coord/size );",
    "gl_FragColor = texel;",

  "}"

].join( "\n" )

let init = false

export default {
  name: 'Base',
  videoSource: null,
  frame: null,
  plane: null,
  scene: null,
  renderer: null,
  camera: null,
  init: function() {

    if(!this.plane || !this.videoSource.texture || !this.scene){
      throw 'ALAAARM! не установлены видеотекстура, сцена и сплейн'
    }

    this.scene.children = []

    const finalMaterial = new THREE.ShaderMaterial( {
    uniforms: {
       texture: { type: "t", value: this.videoSource.texture },
       size: {type: 'v2',value:new THREE.Vector2(this.frame.width, this.frame.height)}
      },
      // vertexShader: vertexShader,
      fragmentShader: fragmentShader
    });

    const quad = new THREE.Mesh( this.plane, finalMaterial );
    this.scene.add(quad);
    init = true
  },
  render: function(renderer) {
    if(!init)return
    renderer.render( this.scene, this.camera );
  }
}