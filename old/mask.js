const MaskInit = function() {
  let mask = document.getElementById('mask')
  let rendererDom = document.getElementById('renderer')

  // режимы
  // рисование маски
  // - очистка
  // - размер кисти
  // наложение маски
  // нет маски


  mask.width = rendererDom.offsetWidth
  mask.height = rendererDom.offsetHeight
  mask.style.top = rendererDom.offsetTop+"px"
  mask.style.left = rendererDom.offsetLeft+"px"

  let active = false

  addEventListener("keydown", function(event) {
    switch(event.keyCode){     
      case 77:  // если нажата клавиша M
        if(!active){
          active = true
          mask.style.opacity = 0.5
        } else {
          active = false
          mask.style.opacity = 0
        }
    }
  })

  let ctx2 = mask.getContext('2d')

  let onMouse = false
  let mouseX = 0;
  let mouseY = 0;

  printCircle2 = (color) => {
    ctx2.beginPath();
    ctx2.fillStyle = color;
    ctx2.arc(mouseX - rendererDom.offsetLeft, mouseY - rendererDom.offsetTop, 30, 0, Math.PI * 2);
    ctx2.fill()
  }
  mask.onmousedown = (ev)=>{
    onMouse = true
  }
  const end = (ev)=>{
    onMouse = false
  }
  mask.onmouseup = end
  mask.onmouseout = end

  mask.onmousemove = (ev)=>{
    mouseX = ev.clientX
    mouseY = ev.clientY
  }

  function draw() {

    // ctx.globalCompositeOperation = 'source-over'

    // ctx.clearRect(0, 0, mask.width, mask.height)
    // ctx.fillStyle = 'rgba(1, 1, 1, 1)';
    // ctx.fillRect(0, 0, mask.width, mask.height)
    // ctx.fillStyle = 'rgba(200, 200, 200, 1)';
    // ctx.arc(50, 50, 30, 0, Math.PI * 2);
    // ctx.fill();

    onMouse && active && printCircle2('rgba(1, 1, 1, 1)')

    window.requestAnimationFrame(draw)
  }
  window.requestAnimationFrame(draw)

}