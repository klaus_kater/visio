
  const fragmentShader = [
      "uniform vec2 size; //The width and height of our screen",
      "uniform sampler2D bufferTexture; //Our input texture",
      "uniform sampler2D sourceTexture; ",
      // "uniform vec3 smokeSource; //The x,y are the posiiton. The z is the power/density",
      // "uniform float time;",
      "void main() {",
          "vec2 pixel = gl_FragCoord.xy / size.xy;",
          "//Get the distance of the current pixel from the smoke source",
          // "float dist = distance(smokeSource.xy,gl_FragCoord.xy);",
          "//Get the color of the current pixel",
          "gl_FragColor = texture2D( bufferTexture, pixel );",
          "gl_FragColor = max(gl_FragColor, texture2D( sourceTexture, pixel ));",

          // "gl_FragColor = texture2D( sourceTexture, pixel );",
          
          "// ",
          "//Generate smoke when mouse is pressed",
          // "gl_FragColor.rgb += smokeSource.z * max(15.0-dist,0.0);",
          "// ",
          "//Generate fixed smoke (this is the little point moving around in the center)",
          // "vec2 smokePoint = vec2(size.x/2.0+100.0*sin(time),size.y/2.0+cos(time*3.5)*20.0);",
          // "dist = distance(smokePoint,gl_FragCoord.xy);",
          // "gl_FragColor.rgb += 0.01 * max(15.0-dist,0.0);",
          "// ",


          "//Smoke diffuse",
          "float xPixel = 1.0/size.x;//The size of a single pixel",
          "float yPixel = 1.0/size.y;",
          "vec4 rightColor = texture2D(bufferTexture,vec2(pixel.x+xPixel,pixel.y));",
          "vec4 leftColor = texture2D(bufferTexture,vec2(pixel.x-xPixel,pixel.y));",
          "vec4 upColor = texture2D(bufferTexture,vec2(pixel.x,pixel.y+yPixel));",
          "vec4 downColor = texture2D(bufferTexture,vec2(pixel.x,pixel.y-yPixel));",
          "//Handle the bottom boundary",
          "if(pixel.y <= yPixel){",
              "downColor.rgb = vec3(0.0);",
          "}",
          "//Diffuse equation",
          "float factor = 8.0 * 0.016 * (leftColor.r + rightColor.r + downColor.r * 3.0 + upColor.r - 6.0 * gl_FragColor.r);",
          "//        ",
          "//Account for low precision of texels",
          "float minimum = 0.003;",
          "if(factor >= -minimum && factor < 0.0) factor = -minimum;",
          "// ",
          "gl_FragColor.rgb += factor;",
          "//           ",
       "}"].join( "\n" )

  let init = false

  // var video
  // var video_texture
  // var maxtex = 2048
  // var width // = Math.round(maxtex/8*1.3)//160 // подставить расчет отношения
  // var height // = Math.round(maxtex/8)//120
  var streaming = false

  var bufferScene;
  var textureA;
  var textureB;
  var sizeTexture;
  var bufferMaterial;
  var bufferObject;
  var finalMaterial;
  var quad;
  var rows;
  var columns;
  var shiftleft = 0;
  var shifttop = 0;
  var time = 0;

  var rows = 8
  var columns = 4

  export default {
    name: 'Smoke',
    // video_texture: null,
    videoSource: null,
    frame: null,
    plane: null,
    scene: null,
    renderer: null,
    camera: null,
    init: function() {

      this.scene.children = []
      
      bufferScene = new THREE.Scene();
      textureA = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});
      textureB = new THREE.WebGLRenderTarget( this.videoSource.width, this.videoSource.height, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter});

      bufferMaterial = new THREE.ShaderMaterial( {
          uniforms: {
            bufferTexture: { type: "t", value: textureA },
            sourceTexture: { type: "t", value: this.videoSource.texture },
            size : {type: 'v2',value:new THREE.Vector2(this.videoSource.width, this.videoSource.height)},
          },
          fragmentShader: fragmentShader
      });
      bufferObject = new THREE.Mesh( this.plane, bufferMaterial );
      bufferScene.add(bufferObject);

      finalMaterial =  new THREE.MeshBasicMaterial({map: textureB});

      quad = new THREE.Mesh( this.plane, finalMaterial );
      this.scene.add(quad);

      init = true
    },
    render: function(renderer) {
      if(!init)return

      //Draw to textureB
      renderer.render(bufferScene,this.camera,textureB,true);
        
      //Swap textureA and B
      var t = textureA;
      textureA = textureB;
      textureB = t;
      quad.material.map = textureB;
      bufferMaterial.uniforms.bufferTexture.value = textureA;

      renderer.render( this.scene, this.camera );
    }
  }
